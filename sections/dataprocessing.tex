\clearpage
\section{Data processing and analysis}
\label{sec:dataprocessing}
%
This study looked at positron showers. The original ntuples were pruned to speed up the analysis processing time, and the following were removed:
%
\begin{itemize}
    \item hits in the hadronic section (layers 29-40)
    \item hits with less deposited energy than half of a MIP
    \item hits detected by the noisy chip \#0 in the first layer (following the reccomendation of the authors of \cite{testbeamdata_paper})
\end{itemize}
%
The energy deposited in the absorber was calculated using the ``dEdX method'', where $dE/dX$ refers to the stopping power for a MIP in the absorber material, being in this case lead plus stainless steel or copper plus tungsten. To estimate the energy deposited on a particular absorber layer $i$, one multiplies an estimate for the number of MIPs in the same absorber layer by an estimate for the energy deposited there per MIP. The former is calculated performing an average of the number of MIPs between the two active layers that lie immediately before and after the absorber layer $i$, and the latter uses the most probable value of the stopping power, as given by the Landau distribution, multiplied by the distance travelled by the MIP in the absorber. The procedure is explained in detail in \cite{testbeamdata_paper}. \par
The input ntuples already included the energy deposited per active layer in units of MIPs. We thus had to perform a conversion to MeVs, using the weights (in MeV/MIP units) provided in Table 3 of the same reference, which we transcribe here for convenience: \par
%
\begin{table}[!htb]
\centering
\begin{tabular}{ |c|c|c|c|c|c|c|c| } 
 \hline
 \textbf{Layer} & Weight & \textbf{Layer} & Weight & \textbf{Layer} & Weight & \textbf{Layer} & Weight \\
 \hline
 \textbf{1} & 11.289 & \textbf{2} & 9.851 & \textbf{3} & 9.851 & \textbf{4} & 9.851 \\
 \hline
 \textbf{5} & 9.851 & \textbf{6} & 9.851 & \textbf{7} & 9.851 & \textbf{8} & 9.851 \\
 \hline
 \textbf{9} & 9.851 & \textbf{10} & 9.851 & \textbf{11} & 9.851 & \textbf{12} & 9.851 \\
 \hline
 \textbf{13} & 9.851 & \textbf{14} & 9.851 & \textbf{15} & 9.851 & \textbf{16} & 9.851 \\
 \hline
 \textbf{17} & 9.851 & \textbf{18} & 9.851 & \textbf{19} & 9.851 & \textbf{20} & 9.851 \\
 \hline
 \textbf{21} & 11.360 & \textbf{22} & 11.360 & \textbf{23} & 11.360 & \textbf{24} & 11.360 \\
 \hline
 \textbf{25} & 10.995 & \textbf{26} & 10.995 & \textbf{27} & 11.153 & \textbf{28} & 7.470 \\
 \hline
\end{tabular}
\caption{Layer-dependent integrated energy loss values used to estimate the energy deposited in the absorber. The values are in MeV/MIP units, and were taken from Table 3 of Ref.~\cite{testbeamdata_paper}.}
\label{table:weights}
\end{table}
%
\par
\noindent The reconstructed energy of an event is given by the sum of all the converted positron-related energy deposits within the first 28 layers of the detector. In addition, CLUE includes two cuts (see Section \ref{sec:clue}):
\begin{itemize}
    \item a low energy cutoff
    \item a cut used for defining the seeds
\end{itemize}
\noindent both of which are defined in multiples of the layer-dependent noise expected for the active region. The latter, according to \cite{testbeamdata_paper}, is estimated to be one-sixth of the energy deposited by a MIP for 300\um{} ($E(\text{MIP})_{\text{dep, 300}}/6$), \textit{i.e.}, $\approx 85/6 \approx 14.3$ KeV. This value must be converted to an energy value comparable to the energies deposited in the absorber; each weight is thus multiplied by approximately 1/6. To be more clear, when checking whether a hit on layer $i$ passes CLUE's first cut, the following condition has to be met, in MeV units:
%
\begin{equation}
E_{\text{hit},i} > \epsilon\times \frac{ 0.0143\; \text{weight}_{i} }{ E(\text{MIP})_{\text{dep}} },
\end{equation}
%
where $\epsilon$ is a tunable parameter for the moment set to 3, and $E(\text{MIP})_{dep}$ represents the energy deposited by a MIP on 300\um{} (86 keV) and in 200\um{} (56.7 keV), depending on the layer being considered. If all the layers had 300\um{} thick silicon, this energy cut would be equivalent to the half MIP cut already applied ($\epsilon = 3$, $3\times 1/6=1/2$). An identical cut, multiplied by a different tunable parameter, select the seeds (see the critical density cut in Section~\ref{sec:clue}). \par
%
A control channel was defined using all the hits in the ntuple that passed the energy cut, without applying any clustering, and was denoted ``reconstructable'', as it corresponds to all the energy that was originally available, except from cleaning and low energy cuts. \par
%
The analysis code used in this work is publicly available at~\cite{mygithub} and runs within CMSSW (11\_1\_0\_pre2 release). The data ntuples were processed using \texttt{ROOT}'s \texttt{RDataFrame} \cite{RDataFrame} and standard \texttt{Python} data processing and analysis tools, such as \texttt{Pandas}\cite{pandas}, \texttt{Numpy}\cite{numpy} and \texttt{Scipy}\cite{scipy}. A custom wrapper around \texttt{Bokeh}\cite{bokeh} was used for (interactive) visualization. \par
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Energy response and resolution}
Both the clustered and the reconstructable datasets are split according to the initial beam energy. Each energy of the reconstructable dataset is separately fit with a gaussian function without constraints; an empirical inspection of each dataset is used to provide a reasonable starting value for the mean ($\mu$), width ($\sigma$) and constant term for each gaussian. Then, following the procedure adopted in \cite{testbeamdata_paper}, the same data is refitted considering the $[\mu - \sigma; \mu + 2.5\sigma]$ range only. This procedure decreases the bias introduced by the low energy tails in the data. The means of this second fit are extracted together with their error, taken directly to be the error of the fit. The fits can be seen in Fig.~\ref{fig:fits}. Please notice the small peak for very low energies for \texttt{sim\_proton}, due to proton contamination; it doe snot affect the analysis due to the constraint applied to the fits. \par
%
\begin{figure}[h]
\centering
\includegraphics[width=0.32\textwidth]{figs/resp_res/data/data_pure_rechit_energy_Ecut_0.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_noproton/sim_noproton_pure_rechit_energy_Ecut_0.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_proton/sim_proton_pure_rechit_energy_Ecut_0.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/data/data_pure_rechit_energy_Ecut_6.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_noproton/sim_noproton_pure_rechit_energy_Ecut_6.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_proton/sim_proton_pure_rechit_energy_Ecut_6.png}
\caption{Example of fits performed to extract the energy repsonse and energy resolution for 20 GeV (top) and 150 GeV (bottom) for the three datasets considered: \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right).}
\label{fig:fits}
\end{figure}
%
In the control (reconstructable) channel, we aim at calibrating the data in order to have a zero electromagnetic response, as defined by: \par
%
\begin{equation}
\text{Response} = \frac{\text{Energy recovered}}{\text{Nominal beam energy}} - 1
\label{eq:response}
\end{equation}
%
We thus fit a straight line to the recovered energy as a function of the nominal beam energy, as shown in Fig.~\ref{fig:calib}, and obtain one single calibration constant. The latter is subsequently used to shift the energy distributions point by point and obtain, by construction, a response close to zero, measured from the mean obtained in the fits. These new shifted means are again obtained only after refitting the data in a narrower range. \par
%
\begin{figure}[h]
\centering
\includegraphics[width=0.45\textwidth]{figs/resp_res/data/data_linear_regressions_0.png}
\includegraphics[width=0.45\textwidth]{figs/resp_res/data/data_linear_regressions_1.png}
\caption{Reconstructable (left) and clustered (right) energy calibration linear regressions. The slopes taken from the fits were used to shift the distributions of the corresponding datasets. The left one was used to define a reference with null response to which CLUE's response could be compared, while the right calibration was solely used to shift the clustered distributions to calculate their resolution. The similarity between the fit's parameters show in a preliminary way that CLUE is reconstructig most of the available energy.}
\label{fig:calib}
\end{figure}
%
Exactly the same fitting procedure is applied to the clustered dataset after shifting it with the calibration constant mentioned above. The measured response can be thus directly compared to the reconstructable one, providing a quantitative measure of CLUE's ability to retrieve the available deposited energy. The fractional energy response, in turn, is calculated by dividing the width $\sigma$ of each gaussian fit by the corresponding nominal beam energy, and is obtained only after reshifting the clustered distributions a second time, so that they get aligned with a null-response; this mimicks the single shift performed to the reconstructable dataset. The similarity between the fit's parameters in Fig.~\ref{fig:calib} suggest that CLUE is reconstructing most of the reconstructable energy. The response and resolutions measured with CLUE are shown in Figs.~\ref{fig:resolution_data} and \ref{fig:resp_data}. Note that the resolution in \texttt{data} is consistently higher than the one in simulations by a factor of approximately 5-10\%. The responses also lie closer to 0 in the simulations. The latter two effects might be attributed to the low energy tail in \texttt{data} didstributions.
%
\begin{figure}[h]
\centering
\includegraphics[width=0.32\textwidth]{figs/resp_res/data/data_responses_and_resolutions_0.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_0.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_0.png}
\caption{Fractional energy resolution of the reconstructable (green) and clusterized (orange) distributions for \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right).}
\label{fig:resolution_data}
\end{figure}
%
\begin{figure}[h]
\centering
\includegraphics[width=0.33\textwidth]{figs/resp_res/data/data_responses_and_resolutions_1.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_1.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_1.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/data/data_responses_and_resolutions_2.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_2.png}
\includegraphics[width=0.32\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_2.png}
\caption{\textit{Top}: Energy response of the reconstructable (green) and clusterized (orange) distributions for \texttt{data} (left), \texttt{sim\_noproton} and \texttt{sim\_proton}. \textit{Bottom}: differences between the former distributions, equivalent to $(E_{\text{reconstructable}} - E_{\text{clusterized}}) / E_{\text{nominal}}$. CLUE loses less than 1\% of the available energy.}
\label{fig:resp_data}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Densities ($\rho$) and distances ($\delta$)}
%
The two fundamental variables for seed definition, the energy density $\rho$ and the distance to the nearest highest density hit $\delta$, are displayed both independently and combined in Figs.~\ref{fig:densities},\ref{fig:distances} and \ref{fig:densities_distances}. By definition, many seeds never get assigned a specific $\delta$, since there are no hits in their vicinity with a larger $\rho$. To better visualize the datasets, we set those distances to 0.5. The counts in the 2D plot were weighted by a factor proportional to $\rho^6$, point by point, in order to make the density dominance of the seeds clear (points lying at a $\delta = 0.5$). For the 1D density plots, we further separated contributions from seeds and followers, after applying an energy cut at 1000 MeV to remove low energy hits. After the energy cut the distributions are dominated by seeds, since most followers lie at low energies. For the \texttt{data} at 50 GeV, it was observed that the fraction of seeds grows from 16\% to 84\% after the cut. It was also observed that those fractions are essentially independent of the number of hits of the clusters each hit belongs to, meaning that the overall behaviour of seeds and followers in terms of densities is not affected by the size of their clusters. \par
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_densities_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_densities_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_densities_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_densities_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_densities_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_densities_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_densities_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_densities_1D_0010.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_densities_1D_0010.png}
\caption{Cluster's 1D $\rho$ densities for 50GeV, for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column), and for layers \#5 (top row), \#8 (middle row) and \#11 (bottom row).}
\label{fig:densities}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_distances_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_distances_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_distances_1D_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_distances_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_distances_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_distances_1D_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_distances_1D_0010.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_distances_1D_0010.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_distances_1D_0010.png}
\caption{Cluster's 1D $\delta$ distances for 50GeV, for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column), and for layers \#5 (top row), \#8 (middle row) and \#11 (bottom row).}
\label{fig:distances}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_dens_vs_dist_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_dens_vs_dist_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_dens_vs_dist_0004.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_dens_vs_dist_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_dens_vs_dist_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_dens_vs_dist_0007.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_dens_vs_dist_0010.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_dens_vs_dist_0010.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_dens_vs_dist_0010.png}
\caption{Cluster's 2D $\rho$ densities vs $\delta$ distances plots for 50GeV, for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column), and for layers \#5 (top row), \#8 (middle row) and \#11 (bottom row).}
\label{fig:densities_distances}
\end{figure}
%
The 2D distributions consistently show that seeds have a larger density than followers, as one would expect. Note that, constrary to the 2D plots, the 1D density plots were not weighted by the energy density. \par
%
The modes for each layer of the 1D density distributions are shown in the same plot (see Fig.~\ref{fig:density_modes}), unveiling an ``oscillation'' pattern that is cross-checked in later stages of this work. The pattern is only present in the data. Only the layers lying close to the shower maximum hould be considered: the others do not have enough available hits.
%
\begin{figure}[h]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_densities_1D_0028.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_densities_1D_0028.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_densities_1D_0028.png}
\caption{Modes of the 1D density distributions shown in Fig.~\ref{fig:densities_distances} as a function of the detector's depth for 50GeV, for \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right). Three categories can be seen: all hits (blue), seeds only (red, which covers the blue line for most points) and followers only (purple). An oscillation behaviour close to the shower maximum exists only for \texttt{data}.}
\label{fig:density_modes}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clusters' hits and energies}
%
The energy and hit content of the clusters, together with their multiplicity, are also studied. Energy cuts are applied whenever the low energy data hides a specific pattern. The data is visualized in a single plot as a function of the layer. Some examples can be seen in Figs.~\ref{fig:morphology_hits}-\ref{fig:morphology_numbers}, where the number of hits per cluster was weighted by the cluster energy. The distributions follow the expected behaviour: the energy distributions shows a clear electromagnetic shower profile. After applying an energy cut at 1000 MeV, the number of clusters is always one, meaning that CLUE was successfully able to recover the single positron shower in each layer.
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_hits_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_hits_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_hits_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_hits_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_hits_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_hits_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_hits_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_hits_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_hits_0008.png}
\caption{Number of hits per cluster for 30 GeV (top row), 100 GeV (middle row) and 250GeV (bottom row), for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column).}
\label{fig:morphology_hits}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_energy_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_energy_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_energy_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_energy_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_energy_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_energy_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_energy_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_energy_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_energy_0018.png}
\caption{Cluster energy for 30 GeV (top row), 100 GeV (middle row) and 250GeV (bottom row), for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column). A cut selecting only cluster with mor ethan 1000 MeV was applied.}
\label{fig:morphology_energy}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0001.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0004.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0008.png}
\caption{Number of hits per cluster for 30 GeV (top row), 100 GeV (middle row) and 250GeV (bottom row), for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column).}
\label{fig:morphology_numbers_withcut}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0014.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_number_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_noproton/sim_noproton_plot_clusters_number_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/sim_proton/sim_proton_plot_clusters_number_0018.png}
\caption{Number of hits per cluster for 30 GeV (top row), 100 GeV (middle row) and 250GeV (bottom row), for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column). A cut selecting only cluster with mor ethan 1000 MeV was applied.}
\label{fig:morphology_numbers}
\end{figure}
%
Before applying the cut (Fig.~\ref{fig:morphology_numbers_withcut}), we observed around 4-6 clusters, which is reasonable given some radiation loss that might occur along the shower longitudinal profile (an ideal case should allow for one cluster only to gater all the energy). The number of hits per cluster tends to increase with nominal beam energy, and again show the expected shower development behaviour, with a peak at around half the electromagnetic section's length. It is important to keep in mind that further particle clustering and identification is done later in \hgcal's reconstruction chain, specifically within TICL. \par
%
A striking pattern becomes once again apparent: an oscillation of the results for \texttt{data} that is absent in the simulations. The oscillation only appears when looking at the number of hits, not at the energy, which seems to suggest that it only affects low energy deposits.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Cluster morphology}
In order to observe the overall shape of the clusters in the detector we wrote a standalone version of the logarithmic weighted position calculation algorithm explained in Ref.~\ref{PositionsArabella}. This further allows testing the algorithm with the testbeam configuration. The $x$ cluster position is calculated as follows (same for $y$):
%
\begin{equation}
   x = \frac{ \sum_i x_i w_i }{ \sum_i w_i }, \: w_i = w_0 + \log\frac{E_i}{\sum_i E_i}
\label{eq:pos}       
\end{equation}
%
\noindent where $i$ refers to a hit and $E_i$ to its energy. Given $w_i \geq 0$, $w_0$ controls the smallest energy fraction that a cell can have to contribute to the position measurement, and was set to 2.9, corresponding to a 5.5\% minimum energy fraction.
%
Fig.~\ref{fig:x} shows $x$ as a function of the layer number with and without and additional energy cut at 1000 MeV. Some noisy low energy cells can be spotted. It is also clear that the beam was not pefectly orthogonal to the layers. This feature was expected and is discussed in Ref.~\cite{ThorbenThesis}. Showing both $x$ and $y$ in the same plot (Fig.~\ref{fig:x_y}) reveals instead an unexpected pattern, namely a strong correlation between the position measurement and the detector's geometry, which is discussed in Section~\ref{sec:results}.
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0018.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_0018.png}
\caption{Cluster's $x$ position for 250GeV for layer \#8, for \texttt{data} (left column), \texttt{sim\_noproton} (middle column) and \texttt{sim\_proton} (right column), without an additional energy cut (top) and with $E_{\text{cluster}} > 1000$MeV (bottom).}
\label{fig:x}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0005.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0005.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0005.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0008.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0011.png}
\includegraphics[width=0.32\textwidth]{figs/cluster_dep/data/data_plot_clusters_posx_vs_posy_0011.png}
\caption{Cluster's $x$ \textit{vs} $y$ positions for 250GeV and layer \#5 (top row) \#8 (middle row) and \#11 (bottom row), for \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right), with $E_{\text{cluster}} > 1000$MeV. A strong correlation between the result and the geometry is visible.}
\label{fig:x_y}
\end{figure}
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Clustered hits and energy ratios}
%
We finally produced Figs.~\ref{fig:hits_ratio} and \ref{fig:energy_ratio} to display the CLUE's overall capabilities to retrieve as a function of the layers, respectively, the number of hits per cluster and the cluster energy. The former provides another cross-check for the alternance pattern seen in the data only, while the pattern is absent in the latter. We thus conclude that the oscillation is due only to low energy hits.
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_hits_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_hits_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_hits_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_hits_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_hits_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_hits_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_hits_fraction_2D_6.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_hits_fraction_2D_6.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_hits_fraction_2D_6.png}
\caption{Ratio of clustered hits for 20 GeV (top row), 80 GeV (middle row) and 150 GeV (top row), for \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right), with $E_{\text{cluster}} > 1000$MeV. A clear alternance pattern is visible between adjacent layers.}
\label{fig:hits_ratio}
\end{figure}
%
\begin{figure}[!hptb]
\centering
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_energy_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_energy_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_energy_fraction_2D_0.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_energy_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_energy_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_energy_fraction_2D_3.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/data/data_energy_fraction_2D_6.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_noproton/sim_noproton_energy_fraction_2D_6.png}
\includegraphics[width=0.32\textwidth]{figs/layer_dep/sim_proton/sim_proton_energy_fraction_2D_6.png}
\caption{Ratio of clustered energy for 20 GeV (top row), 80 GeV (middle row) and 150 GeV (top row), for \texttt{data} (left), \texttt{sim\_noproton} (middle) and \texttt{sim\_proton} (right), with $E_{\text{cluster}} > 1000$MeV. The pattern visible in \ref{fig:hits_ratio} is not visible here.}
\label{fig:energy_ratio}
\end{figure}
%
