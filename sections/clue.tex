%\clearpage
\section{CLUE algorithm}
\label{sec:clue}
%
The CLUstering of Energy (\clue{}) \cite{CLUE}, inspired by \cite{ImageAlgo}, is a density-based clustering algorithm developed having both parallelization and a tight time constraint in mind, to avoid bottlenecking the entire reconstruction within \hgcal. It takes calibrated hits (RecHits) as input and produces 2D layer clusters, which are subsequently feeded into TICL to form 3D objects or showers which are further processed. Among the plethora of available RecHits, and using a spatial fixed-grid, CLUE defines and calculates the following two key values for each individual hit:
%
\begin{itemize}
\item $\rho$ [MeV]: local energy density (it is normalized by construction, since the sizes of the search boxes are the same for every hit)
\item $\delta$ [cm]: distance to the nearest point with the highest density (within a fixed-size search box)
\end{itemize}
%
In order to define the cluster's seed hits, corresponding follower hits, and outliers, the following conditions are required within CLUE:
%
\begin{itemize}
\item Seeds: $\delta_{\text{hit}} > d_c$ and $\rho_{\text{hit}} >= \rho_c$
\item Outliers: $\delta_{\text{hit}} > 2d_c$ and $\rho_{\text{hit}} < \rho_c$
\item Followers: hits that are neither seeds nor outliers (they ``follow'' a seed)
\end{itemize}
%
where three tunable parameters are introduced:
%
\begin{itemize}
\item $\rho_c = \kappa \times \text{noise}$ [MeV]: where $\kappa$ is tunable and set to 9, and $\rho_c$ is the critical density;
\item $d_c$ [cm]: cut-off distance for the calculation of the local density, set to 1.3 cm (this value allows to capture hits deposited in the central cell plus its 6 close surrounding neighbours, in both high and low density configurations);
\item $E_{cut} = \epsilon \times \text{noise}$ [MeV]: cut-off energy value, where $\epsilon$ is tunable and set to 3.
\end{itemize}
%
The parameter denoted by ``noise'' refers to the electronic noise of the detector, and its calculation is described in Section~\ref{sec:dataprocessing}. \par
%
The algorithm first calculates $\rho$ for each hit using other hits within a pre-defined spatial search box of side $d_c$, then uses this value to calculate $\delta$ (for each individual hit), using a search box of size $2d_c$. It later assigns each hit to a particular cluster, as long as it is not an outlier, following the density gradient created by all the the $\delta$ values. Some hits initially classified as seeds can nevertheless be followers of other higher-density hits. The number of clusters to be reconstructed is not fixed, and is determined by the number of seeds at the last stage of the algorithm. For more details, please see \cite{CLUE} and \href{http://hgcal.web.cern.ch/hgcal/Reconstruction/clueAlgorithm/}{\hgcal's website}. \par
%
A standalone version of CLUE was used, available in the analsysis repository, to remove CMSSW complexity from this study. Both versions should be identical. Some utility functions were added to ease some processing steps. \par
%
\subsection{CLUE performance with simulated data}
The algorithm had been tested using simulated samples produced with the official CMSSW software, and its performance looked promising, as one can see in Fig.~\ref{fig:officialsim_results}. However, testing it with testbeam \textit{data} allows for a much more realistic quality assessment.
%
\begin{figure}[h]
\centering
\includegraphics[width=.5\textwidth]{figs/SimResponse.png}
\caption{Energy response for reconstructable (black) and clustered (pink) hits, and for hits processed with TICL (blue), using electromagnetic showers. The pink points were obtained with CLUE, and show an almost perfect match with the reconstructable energy. The figure was taken from \cite{CLUEOfficialSim}.}
\label{fig:officialsim_results}
\end{figure}

