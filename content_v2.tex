\begin{frame}[plain] %plain hides the frame's number
  \titlepage
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Testbeam data used}

    \begin{itemize}
        \item Configuration \#22 (October 2018): 28 CE-E (1 Si module/layer) + 12 CE-H layers (the first 9 have 7 modules/layer and the last 3 have 1 module/layer)
        \item Nominal beam energies:

        \begin{itemize}
            \item (20, 30, 49.99, 79.93, 99.83, 119.65, 149.14, 197.32, 243.61, 287.18) GeV
        \end{itemize}

        \item Only electron showers were considered
        \item Data cleaning:

        \begin{itemize}
            \item Consider the first 28 layers only
            \item Runs with run number under 424 were ignored due to ROC configuration issues
            \item Hits must have an energy above 0.5 MIPs \highlight{[new]}
            \item All hits coming from a defective chip on the first layer were removed \highlight{[new]}
        \end{itemize}        
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUstering of Energy algorithm (CLUE)}
    Density-based clustering algorithm developed with parallelization and a tight time constraint in mind \cite{CLUE}. It does not require a fixed number of clusters to reconstruct. See slides~\ref{slide:clue2},\ref{slide:clue3} and~\ref{slide:clue4} for more details. \par

    \begin{figure}[H]
        \includegraphics[width=0.9\textwidth]{figs/ticl_chain.png}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Changes since last presentation}

    \begin{itemize}
        \item Repeat the analysis using testbeam simulation data
        \item Calculate and plot positions of the \highlight{clusters}
        \item Plot ratios of clusterized hits and energies per event
        \item Compare the density between seeds and followers
        \item Solve visualization bug with the CLUE densities \textit{vs} CLUE distances plot 
        \item (Write the data selection and CLUE analysis as a HTCondor Directed Acyclic Graph; make it trivial to add a new dataset to the code)
    \end{itemize}


    The following samples were considered:
    \begin{itemize}
        \item \texttt{data}: {\scriptsize \textit{/eos/.../offline\_analysis/ntuples/v16} }
        \item \texttt{sim\_noproton}: {\scriptsize \textit{/eos/.../offline\_analysis/sim\_ntuples/../v5/electrons} } (no proton contamination)
        \item \texttt{sim\_proton}: {\scriptsize \textit{/eos/.../offline\_analysis/sim\_ntuples/../v3/electrons} } (with proton contamination)
    \end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Gaussian fits}

  \begin{itemize}
      \item All the fits converged:
      \begin{itemize}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/resp_res/data/?match=*Ecut*&depth=1}{\texttt{data}}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/resp_res/sim_noproton/?match=*Ecut*&depth=1}{\texttt{sim\_noproton}} 
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/resp_res/sim_proton/?match=*Ecut*&depth=1}{\texttt{sim\_proton}} (there is a \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/resp_res/sim_proton/?match=*zero*&depth=1}{small peak} at very low energies)
      \end{itemize}
      \item The added cleaning cuts effectively removed the observed double peak at 20 GeV for the data
      \item The extra cuts are also responsible for better resolution and response results
      \item The calibration plots can be seen in slides~\ref{slide:calib_data}, \ref{slide:calib_sim_noproton} and \ref{slide:calib_sim_proton}.
  \end{itemize}
  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Results: resolution \& response (\texttt{data})}
  
    \begin{figure}[H]
        \includegraphics[width=0.28\textwidth]{figs/resp_res/data/data_responses_and_resolutions_0.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/data/data_responses_and_resolutions_1.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/data/data_responses_and_resolutions_2.png}
        \label{fig:res_resp}
        \caption{Comparison between reconstructable and clusterized energy. \textit{Left)} Fractional resolution. \textit{Middle)} Energy response \textit{Right)} Difference between responses.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Results: resolution \& response (\texttt{sim\_noproton})}
  
    \begin{figure}[H]
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_0.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_1.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_noproton/sim_noproton_responses_and_resolutions_2.png}
        \label{fig:res_resp}
        \caption{Comparison between reconstructable and clusterized energy. \textit{Left)} Fractional resolution. \textit{Middle)} Energy response \textit{Right)} Difference between responses.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Results: resolution \& response (\texttt{sim\_proton})}
  
    \begin{figure}[H]
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_0.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_1.png}
        \includegraphics[width=0.28\textwidth]{figs/resp_res/sim_proton/sim_proton_responses_and_resolutions_2.png}
        \label{fig:res_resp}
        \caption{Comparison between reconstructable and clusterized energy. \textit{Left)} Fractional resolution. \textit{Middle)} Energy response \textit{Right)} Difference between responses.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Density $\rho$ per hit}

  \begin{itemize}
      \item A wave pattern can be discerned: the density increases up to around the shower maximum, and later decreases.
      \item Plots for 50 GeV available:
      \begin{itemize}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_densities_*&depth=1}{\texttt{data}}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_noproton/?match=*_densities_*&depth=1}{\texttt{sim\_noproton}} 
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_proton/?match=*_densities_*&depth=1}{\texttt{sim\_proton}}
      \end{itemize}
      \item Further test showed that the distribution of the seeds and followers' density is independent of the number of hits of the clusters considered  
  \end{itemize}
  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Distance $\delta$ per hit}

  \begin{itemize}
      \item Seeds were set to 0.5cm for ease of visualization.
      \item Plots for 50 GeV available:
      \begin{itemize}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_distances_*&depth=1}{\texttt{data}}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_noproton/?match=*_distances_*&depth=1}{\texttt{sim\_noproton}} 
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_proton/?match=*_distances_*&depth=1}{\texttt{sim\_proton}}
      \end{itemize}
  \end{itemize}
  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: $\rho$ vs. $\delta$}
  A comment during the last meeting regarding \highlight{followers being more energetic than seeds} lead to the discovery of an interesting bug:

    \begin{figure}[H]
        \includegraphics[width=0.45\textwidth]{figs/dens_vs_dist_old.png}
        \includegraphics[width=0.45\textwidth]{figs/layer_dep/data/data_dens_vs_dist_23.png}
        \label{fig:dens_vs_dist_bug}
        \caption{CLUE energy density vs. CLUE distances to the nearest highest density. \textit{Left)} Old buggy plot \textit{Right)} Correct visualization}
    \end{figure}
      
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: $\rho$ vs. $\delta$}

  \begin{itemize}
      \item The $\delta$ of the seeders was set to 0.5cm. All entries were weighted by a power of the energy density $\rho$
      \item Plots for 50 GeV available:
      \begin{itemize}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_dist_*&depth=1}{\texttt{data}}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_noproton/?match=*_dist_*&depth=1}{\texttt{sim\_noproton}} 
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_proton/?match=*_dist_*&depth=1}{\texttt{sim\_proton}}
      \end{itemize}
  \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Number of hits}

    \begin{itemize}
        \item All entries were weighted by the energy density $\rho$
        \item Cuts at 1000MeV in the energy of the clusters were also applied
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot**clusters_hits_&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_noproton/?match=*plot**clusters_hits_&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_proton/?match=*plot**clusters_hits_&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
        \item Interestingly, the results oscillate in a layer-dependent way in the data, but not in both simulations.
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Cluster's energy}
  
    \begin{itemize}
        \item Cuts at 1000MeV in the energy of the clusters were also applied
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot**energy_&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_noproton/?match=*plot**energy_&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_proton/?match=*plot**energy_&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Number of clusters}

  \begin{itemize}
       \item Cuts at 1000MeV in the energy of the clusters were also applied
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot**number_&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_noproton/?match=*plot**number_&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_proton/?match=*plot**number_&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: positions}

    \begin{itemize}
        \item Logarithmic energy weighted position estimate
        \item Calculation of the X position (same for Y), where $i$ refers to a hit and $E_i$ to its energy:
            \begin{equation*}
                X = \frac{ \sum_i x_i w_i }{ \sum_i w_i }, w_i = w_0 + \log\frac{E_i}{\sum_i E_i}
            \end{equation*}
        \item Given $w_i \geq 0$, $w_0$ controls the smallest energy fraction that a cell can have to contribute to the position measurement
        \item $w_0$ was set to 2.9, corresponding to a 5.5\% minimum energy fraction
        \item This algorithm was introduced in a \href{https://indico.cern.ch/event/776790/contributions/3230583/attachments/1761511/2865143/positionResolutionFor2DClusters_amartell_29Nov.pdf}{presentation} from Arabella.
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: X position}

  \begin{itemize}
        \item Some low energy noisy cells were spotted
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot*posx*&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_noproton/?match=*plot*posx*&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_proton/?match=*plot*posx*&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Y position}

  \begin{itemize}
        \item Some low energy noisy cells were spotted
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot*posx_vs_posy_\%3F\%3F\%3F\%3F.png&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_noproton/?match=*plot*posy*&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/sim_proton/?match=*plot*posy*&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: X \textit{vs.} Y position}

    \begin{itemize}
      \item The position measurement is strongly correlated with the detector's geometry
      \item Requires further study
      \item Example:
      \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/cluster_dep/data/?match=*plot*posy*&depth=1}{\texttt{data}}
      \end{itemize}
    \end{itemize}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Clusterized hits ratio}

  \begin{itemize}
        \item Events without hits for a specific layer were assigned to -0.1 for ease of visualization
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*hits_fraction*&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_noproton/?match=*hits_fraction*&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_proton/?match=*hits_fraction*&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
        \item A clear layer-dependent pattern is again observed in the data.
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Clusterized energy ratio}

  \begin{itemize}
        \item Events without energy for a specific layer were assigned to -0.1 for ease of visualization
        \item Plots for 50 GeV available:
        \begin{itemize}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*energy_fraction*&depth=1}{\texttt{data}}
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_noproton/?match=*energy_fraction*&depth=1}{\texttt{sim\_noproton}} 
            \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/sim_proton/?match=*energy_fraction*&depth=1}{\texttt{sim\_proton}}
        \end{itemize}
        \item The results for the data seems slightly worse than the ones for the simulations
    \end{itemize}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Conclusions}

    \begin{alertblock}{Using $k=9$, $\epsilon=3$ and $d_{c}=1.3$}
        \begin{itemize}
            \item In general CLUE provides good and consistent results for the three datasets considered
            \item The layer-dependent pattern in the data should be understood
            \item The calculation of the cluster's positions is strongly correlated with the geometry
        \end{itemize}
    \end{alertblock}

    \begin{alertblock}{Possible next steps}
        \begin{itemize}
            \item The algorithm that calculates the cluster's positions must be further studied with testbeam data, since it will be used in HGCAL's reconstruction chain
            \item Perform a similar analysis with CLUE applied to hadronic (pion) showers
        \end{itemize}
    \end{alertblock}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}

  \printbibliography
  
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%APPENDIX%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{ROC configuration issue illustrated}
  
    \begin{figure}[H]
        \includegraphics[width=0.45\textwidth]{figs/run22.png}
        \includegraphics[width=0.45\textwidth]{figs/run23.png}
        \label{fig:ROC_issue}
        \caption{Total energy per event \textit{Left)} setup id \#22 \textit{Right)} setup id \#23.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUstering of Energy algorithm (CLUE): Algorithm explanation}
\label{slide:clue2}

    \begin{alertblock}{Steps}
        \begin{itemize}
            \item Calculates density $\rho$ for each hit using other hits within a pre-defined spatial grid of size $d_c = 1.3$ cm;
            \item Calculates the distance to the nearest point with the highest density, $\delta$, within a search box of size $2d_c$;
            \item It finally assigns each hit to a particular cluster, following a density gradient created by the $\delta$ values.
        \end{itemize}
    \end{alertblock}
        
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUstering of Energy algorithm (CLUE): Algorithm explanation}
\label{slide:clue3}
    \begin{columns} 
        \begin{column}{0.4\textwidth}
             A hit can be classified as:

            \begin{itemize}
                \item Seed: $\delta_{\text{hit}} > d_c$ and $\rho_{\text{hit}} >= \rho_c$
                \item Outlier: $\delta_{\text{hit}} > 2d_c$ and $\rho_{\text{hit}} < \rho_c$
                \item Follower: hits that are neither seeds nor outliers (they ``follow'' a seed)
            \end{itemize}

        \end{column}

        \begin{column}{0.6\textwidth}  %%<--- here
            \begin{center}
                 \includegraphics[width=0.8\textwidth]{figs/clue_types.png}
            \end{center}
        \end{column}
    \end{columns}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUstering of Energy algorithm (CLUE): Algorithm explanation}
\label{slide:clue4}
Three tunable parameters are introduced:

    \begin{itemize}
        \item $\rho_c = \kappa \times \text{noise}$ [MeV]: where $\kappa$ is tunable and set to 9, and $\rho_c$ is the critical density;
        \item $d_c$ [cm]: cut-off distance for the calculation of the local density, set to 1.3 cm.
        \item $E_{cut} = \epsilon \times \text{noise}$ [MeV]: cut-off energy value, where $\epsilon$ is tunable and set to 3.
    \end{itemize}
    
    The noise contribution, given by Thorben ($\approx 14.3$ keV in 300 $\mu m$), is weighted by the dEdX method.
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Summary of quantities of interest}

    \begin{alertblock}{Energy response \& resolution}
        \begin{itemize}
            \item Energy resolution as given by gaussian fits
            \item Energy response:
                \begin{align*} %the asterisk removes the equation number
                    \frac{\text{Energy recovered}}{\text{Nominal beam energy}} - 1
                \end{align*}
         \end{itemize}
    \end{alertblock}

    \begin{alertblock}{CLUE related quantities}
        \begin{itemize}
            \item Density $\rho$ and distance $\delta$ calculated for each hit per layer
            \item \#Hits, Hit's energy and \# clusters, per cluster and per layer
            \item Cluster's \texttt{x} and \texttt{y} positions
            \item Ratios of clusterized hits and energies
        \end{itemize}
    \end{alertblock}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Energy calibration (\texttt{data})}
\label{slide:calib_data}
  
    \begin{figure}[H]
        \includegraphics[width=0.45\textwidth]{figs/resp_res/data/data_linear_regressions_0.png}
        \includegraphics[width=0.45\textwidth]{figs/resp_res/data/data_linear_regressions_1.png}
        \label{fig:calib}
        \caption{Linear regressions \textit{Left)} Rec'ble energy \textit{Right)} Clusterized energy.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Energy calibration (\texttt{sim\_noproton})}
\label{slide:calib_sim_noproton}
  
    \begin{figure}[H]
        \includegraphics[width=0.45\textwidth]{figs/resp_res/sim_noproton/sim_noproton_linear_regressions_0.png}
        \includegraphics[width=0.45\textwidth]{figs/resp_res/sim_noproton/sim_noproton_linear_regressions_1.png}
        \label{fig:calib}
        \caption{Linear regressions \textit{Left)} Rec'ble energy \textit{Right)} Clusterized energy.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Energy calibration (\texttt{sim\_proton})}
\label{slide:calib_sim_proton}
  
    \begin{figure}[H]
        \includegraphics[width=0.45\textwidth]{figs/resp_res/sim_proton/sim_proton_linear_regressions_0.png}
        \includegraphics[width=0.45\textwidth]{figs/resp_res/sim_proton/sim_proton_linear_regressions_1.png}
        \label{fig:calib}
        \caption{Linear regressions \textit{Left)} Rec'ble energy \textit{Right)} Clusterized energy.}
    \end{figure}
    
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Response \& Resolution: procedure}
\label{slide:recble}
All energies are calculated using the dEdX method.

    \begin{alertblock}{Reconstructable energy: all RecHits hits that pass $E_{cut}$}
      \begin{enumerate}
            \item Fit the gaussian peak for each nominal beam energy (slide~\ref{slide:fits1}): once with free parameters, and again in the $[\mu - \sigma; \mu + 2.5\sigma]$ range. This procedure decreases the bias due to the low energy tails.
            \item Calibration: linear regression between rec'ble and nominal energies (slide~\ref{slide:calib}, left)
            \item Shift the rec'ble energy distributions by the fixed slope taken from the previous bullet
            \item Calculate the energy response
            \item Calculate the fractional energy resolution ($\sigma/E$)
        \end{enumerate}
    \end{alertblock}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Response \& Resolution: procedure}
\label{slide:clust}
    \begin{alertblock}{Clusterized energy (as retrieved by CLUE)}
        \begin{enumerate}
            \setcounter{enumi}{5}
            \item Follow steps 1-4 using the same shift factor.
            \item Calibration: create a second shifted distribution, to align the clusterized energy with the nominal one (right plot in slides~\ref{slide:calib_data},~\ref{slide:calib_sim_noproton} and \ref{slide:calib_sim_proton}.)
            \item Calculate the resolution after the shift in bullet \#7
        \end{enumerate}
    \end{alertblock}
%
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{CLUE Results: Density $\rho$ per hit as a function of the size of the clusters}

  \begin{itemize}
      \item The shape of the densities for seeds or followers is independent of the number of hits of the clusters to which they belong
      \item Plots for 50 GeV available:
      \begin{itemize}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_densities_1D_\%3F\%3F\%3F\%3F.png&depth=1}{\texttt{data}, all hits}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_densities_morethan1*&depth=1}{\texttt{data}, > 1 hit} 
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_densities_morethan5*&depth=1}{\texttt{data}, > 5 hits}
          \item \href{https://bfontana.web.cern.ch/bfontana/TestBeamReconstruction/layer_dep/data/?match=*_densities_morethan10*&depth=1}{\texttt{data}, > 10 hits}
      \end{itemize}
      \item The above tests show that the distribution of the seeds and followers' density is independent of the number of hits of the clusters considered  
  \end{itemize}
  
\end{frame}
