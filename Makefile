detectornote:
	latexmk -pdf CLUEperformancenote.tex

presentation:
	latexmk -xelatex --shell-escape CLUEpresentation.tex
